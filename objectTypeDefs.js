// core modules required
const path = require('path');
const fs = require('fs');

//initialise an array that will be exported eventually
let typedefs = [];

//read the contents of the objectTypes directory
let typeDefsToAdd = fs.readdirSync(path.join(__dirname, 'objectTypes'));

//add the type defs into the array to export
typeDefsToAdd.forEach(filename => {
    typedefs.push(require(path.join(__dirname, 'objectTypes', filename)));
});

//export it
module.exports = typedefs;
