module.exports = {
    name: 'triggerArgs',
    properties: [
        {
            property: 'acclimatisationOptions',
            type: '<acclimatisationOptions>',
            required: true,
        },
        {
            property: 'numFrames',
            description: 'Number of frames the cameras should capture.',
            type: 'int',
            min: 5,
            max: 32,
            required: true,
        },
        {
            property: 'startStage',
            type: 'int',
            min: 1,
            max: 32,
            required: true,
        },
        {
            property: 'leadingOrFollowing',
            type: 'option',
            options: ['leading', 'following'],
            required: false,
            default: 'following',
        },
        {
            property: 'triggerDuration',
            type: 'int',
            min: 1,
            max: 1000,
            required: false,
            custom: (theParent, thisObj) => {
                if (theParent.hasOwnProperty('leadingOrFollowing')) {
                    if (theParent.leadingOrFollowing === 'following') {
                        return ['Can only set triggerDuration when leadingOrFollowing is set to "leading"'];
                    } else {
                        return [];
                    }
                } else {
                    return ['Can only set triggerDuration when leadingOrFollowing is set to "leading"'];
                }
            },
        },
    ],
};
