module.exports = {
    name: 'acclimatisationOptions',
    properties: [
        {
            property: 'intensity',
            type: 'int',
            min: 1,
            max: 100,
            required: true,
        },
        {
            property: 'duration',
            type: 'int',
            min: 1,
            max: 10000,
            required: true,
        },
    ],
};
